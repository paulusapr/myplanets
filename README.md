## How to run

1. Using terminal

`git@gitlab.com:paulusapr/myplanets.git && cd myplanets && npm install && npm run start`

hit our application in a browser at http://localhost:3000/

## Application Stack

1. [ReactJs](https://reactjs.org/) A JavaScript library for building user interfaces.
2. [Axios](https://www.npmjs.com/package/axios) Promise based HTTP client for the browser and node.js.
3. [Eslint](https://eslint.org/) JavaScript codes linter.
4. [SweetAlert2](https://sweetalert2.github.io//)

<h4 align="center">
    <a href="https://myplanets-paulusapr.vercel.app/">Live Demo</a>
</h4>