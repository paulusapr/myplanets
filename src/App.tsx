import { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Store from './redux/store';
import './App.css';

const List = lazy(() => import('./pages'));
const WishList = lazy(() => import('./pages/wishlist'));

function App() {
  return (
    <div className="App">
      <Store>
        <Router>
          <Suspense fallback={<div>Loading...</div>}>
            <Routes>
              <Route path="/" element={<List />} />
              <Route path="/wishlist" element={<WishList />} />
            </Routes>
          </Suspense>
        </Router>
      </Store>
    </div>
  );
}

export default App;
