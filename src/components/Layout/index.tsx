import React from 'react';
import styled from 'styled-components';

const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const Layout = ({ children }: { children: React.ReactNode }) => (
  <LayoutContainer>
    {children}
  </LayoutContainer>
);

export default Layout;
