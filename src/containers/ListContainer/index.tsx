import { useEffect, useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Swal from 'sweetalert2';
import { Layout } from '../../components';
import { getPlanets } from '../../service/planets';
import { formatFullDate } from '../../utils/common';
import { Context } from '../../redux/store';

const Table = styled.div`
  width: 100%;
  max-width: 1000px;
  margin-left: auto;
  margin-right: auto;
  padding-left: 10px;
  padding-right: 10px;
  .responsive-table {
    padding: 0;
    li {
      border-radius: 3px;
      padding: 25px 30px;
      display: flex;
      justify-content: space-between;
      margin-bottom: 25px;
    }
    .table-header {
      background-color: #95A5A6;
      font-size: 14px;
      text-transform: uppercase;
      letter-spacing: 0.03em;
    }
    .table-row {
      background-color: #ffffff;
      box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.1);
    }
    .col-1 {
      flex-basis: 10%;
      cursor: pointer;
    }
    .col-2 {
      flex-basis: 40%;
    }
    .col-3 {
      flex-basis: 25%;
    }
    .col-4 {
      flex-basis: 25%;
    }
    
    @media all and (max-width: 767px) {
      .table-header {
        display: none;
      }
      .table-row{
        
      }
      li {
        display: block;
      }
      .col {
        
        flex-basis: 100%;
        
      }
      .col {
        display: flex;
        padding: 10px 0;
        &:before {
          color: #6C7A89;
          padding-right: 10px;
          content: attr(data-label);
          flex-basis: 50%;
          text-align: right;
        }
      }
    }
`;

const Button = styled.button`
  border-radius: 4px;
  background: transparent;
  color: red;
  border: 1px solid red;
  cursor: pointer;
  padding: 10px;
`;

const ButtonWish = styled.button`
  border-radius: 4px;
  background: transparent;
  color: #006fff;;
  border: 1px solid #006fff;;
  cursor: pointer;
  padding: 10px;
`;

type typeList = {
  url: string,
  created: Date,
  name: string
};

const ListContainer = () => {
  const state = useContext(Context);
  const { wishlist, setWishlist } = state;
  const [list, setList] = useState([]);
  const push = useNavigate();

  const setToWish = (myWish: any) => {
    const wish = wishlist;
    if (wish.find((el: any) => el.url === myWish.url)) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Planet remove to wishlist',
        showConfirmButton: false,
        timer: 1500,
      });
      setWishlist(wish.filter((el: any) => el.url !== myWish.url));
    } else {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Planet add to wishlist',
        showConfirmButton: false,
        timer: 1500,
      });
      wish.push(myWish);
      setWishlist(wish);
    }
  };

  const getData = () => {
    getPlanets().then((res) => {
      if (res && res.data && res.data.results) {
        setList(res.data.results);
      }
    });
  };

  const showDetail = (detail: any) => {
    Swal.fire(`${detail.name} have climate(s) ${detail.climate}, 
      ${detail.terrain}. Have diameter ${detail.diameter} kilometers and gravity ${detail.gravity}.
      The orbital period is ${detail.orbital_period} and the amount of populations are ${detail.population}.
    `);
  };

  useEffect(() => {
    if (list.length <= 0) getData();
  }, [list]);

  return (
    <Layout>
      <Table>
        <ul className="responsive-table">
          <li className="table-header">
            <div className="col col-1">Name</div>
            <div className="col col-2">Create Date</div>
            <div className="col col-3">
              <ButtonWish onClick={() => push('/wishlist')}>Go to my Wishlist</ButtonWish>
            </div>
          </li>
          {list.map((el: typeList) => (
            <li
              className="table-row"
              key={el.url}
            >
              <div
                className="col col-1"
                data-label="Name"
                onClick={() => showDetail(el)}
                role="button"
                tabIndex={0}
                onKeyUp={() => showDetail(el)}
              >
                {el.name}
              </div>
              <div className="col col-2" data-label="Create Date">{formatFullDate(el.created)}</div>
              <div className="col col-3" data-label="Action">
                <Button onClick={() => setToWish(el)}>Add to wishlist</Button>
              </div>
            </li>
          ))}
        </ul>
      </Table>
    </Layout>
  );
};

export default ListContainer;
