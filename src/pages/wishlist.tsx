import WishListContainer from '../containers/WishListContainer';

const WishListPage = () => <WishListContainer />;

export default WishListPage;
