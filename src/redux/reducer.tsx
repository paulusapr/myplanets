type Action = {
  type: string,
  payload: Array<string | number | Object>,
}

type State = {
  wishlist: Array<string | number | Object>,
};

const Reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'SET_WISHLIST':
      return {
        ...state,
        wishlist: action.payload,
      };
    default:
      return state;
  }
};
export default Reducer;
