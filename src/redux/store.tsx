import React, { createContext, useReducer } from 'react';
import Reducer from './reducer';

type State = {
  wishlist: Array<string | number | Object>,
  setWishlist: Function
};

const initialState: State = {
  wishlist: [],
  setWishlist: () => {},
};

const Store = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  const { wishlist } = state;

  const setWishlist = (payload: Array<string | number | Object>) => {
    dispatch({ type: 'SET_WISHLIST', payload });
  };

  return (
    <Context.Provider value={{ wishlist, setWishlist }}>
      {children}
    </Context.Provider>
  );
};
export const Context = createContext(initialState);
export default Store;
