import apiClient from '../apiClient';

export const getPlanets = async () => {
  const result = await apiClient('get', 'https://swapi.dev/api/planets', {}, {});

  return result;
};
