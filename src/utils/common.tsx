const month = (mon: number) => {
  const Bulan = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return Bulan[mon - 1];
};

const formatFullDate = (date: Date = new Date()) => {
  const d = new Date(date);
  return `${d.getDate()} ${month(d.getMonth())} ${d.getFullYear()}`;
};

export {
  formatFullDate,
};
